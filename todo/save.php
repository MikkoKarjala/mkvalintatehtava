<?php require_once 'inc/top.php'; ?>
<h3>Tallenna</h3>
<?php
try {
    $description = filter_input(INPUT_POST, 'task', FILTER_SANITIZE_STRING);
    $query = $db->prepare("INSERT INTO TASK (description) VALUES (:description);");
    $query->bindValue(':description', $description,PDO::PARAM_STR);
    $query->execute();
    print "<p>Tehtavä lisätty!</p>";
}
catch (PDOException $pdoex) {
    print "<p>Tietojen tallennus epäonnistui. " . $pdoex->getMessage() . "</p>";
}
?>
<a href="index.php">Tehtävälista</p>
<?php require_once 'inc/bottom.php'; ?>