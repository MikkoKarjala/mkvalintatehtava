CREATE TABLE task  (
    id int auto_increment primary key,
    description VARCHAR(255) not null,
    done boolean DEFAULT false,
    added timestamp DEFAULT current_timestamp
)